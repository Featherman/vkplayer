﻿ function play(player){
			var playBut = document.getElementById('Button-Play');
			if(player.getAttribute('src')!=''&&!$(player).hasClass('active')){
				player.play();
				$(player).addClass('active');
				$(playBut).css('background-image',"url('css/img/pause.png')");
			}else{
				if($(player).hasClass('active')){
					player.pause();
					$(player).removeClass('active');
					$(playBut).css('background-image',"url('css/img/play.png')");
				}
			
			}
		
		}	
//Получает рандомное число в заданном диапозоне
 function getRandomInt(min, max)
{
 
  return Math.floor(Math.random() * (max - min + 1)) + min;
 
}
// Обработчик для <audio> 
	 document.getElementById('player_back').addEventListener('timeupdate', function(){
		 var player = this;
		 // Показывает прогресс песни
		   if(chrome.extension.getViews({type: "popup"}).length!=0){
                 	 //get current time in seconds
				var popup=chrome.extension.getViews({type: "popup"})[0].document;
                var elapsedTime = Math.round(player.currentTime);
                //update the progress bar
				var canvas = popup.getElementById('progress');
                if (canvas.getContext) {
                    var ctx = canvas.getContext("2d");
                    //clear canvas before painting
                    ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
                    ctx.fillStyle = "rgb(255,0,0)";
                    var fWidth = (elapsedTime / player.duration) * (canvas.clientWidth);
                    if (fWidth > 0) {
                        ctx.fillRect(0, 0, fWidth, canvas.clientHeight);
                    }
                }
				popup.getElementById('current_time').innerHTML=secToMinute(player.currentTime);
		 }
		 // воспроизводит следующую песню, если эта уже закончилась
		 if(player.ended){
		// Проверяется стоит ли галочка на рандомном воспроизведениее
			 if($(document).find('#random').attr('checked')){
				var i = getRandomInt(0, $(document).find('.song').length-1);
				var nextSong=$(document).find('.song')[i];
			 }else{
				 var currentSong = $(document).find('.active_song')[0];
				var nextSong = $(currentSong).nextAll('.song')[0];
			 }
	  
			// var id = player.getAttribute('src');
			
			 
			 var songId = nextSong.id;
			// alert(nextSong.innerHTML);
			 if(nextSong!=undefined){
				 $($(document).find('.active_song')[0]).removeClass('active_song');
				 var url = $(nextSong).find('.songURL')[0].value;			 
				 $(nextSong).addClass('active_song');
				 player.setAttribute('src', url);
				 $(player).removeClass('active');
				 play(player);
			}
			
			 if(chrome.extension.getViews({type: "popup"}).length!=0){
		
				var popup=chrome.extension.getViews({type: "popup"})[0].document;
				$(popup).find('.active_song').removeClass('active_song');
				var nextSongPop = popup.getElementById(songId);
				//alert(nextSongPop.innerHTML);
				$(nextSongPop).addClass('active_song');
				$(popup).find('.box').each(function(){
					var song=$(popup).find('.active_song')[0];
					if(song!=undefined){
						this.scrollTop= song.offsetTop-134;
					}
					
				 });
				
			 }
		}
	});
 
	