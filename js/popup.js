﻿ 
/**
 * Display an alert with an error message, description
 *
 * @param  {string} textToShow  Error message text
 * @param  {string} errorToShow Error to show
 */
  //Необходимая функция для авторизации
function displayeAnError(textToShow, errorToShow) {
    "use strict";

    alert(textToShow + '\n' + errorToShow);
}

/**
 * Retrieve a value of a paraameter from the given URL string
 *
 * @param  {string} url           Url string
 * @param  {string} parameterName Name of the parameter
 *
 * @return {string}               Value of the parameter
 */
 //Необходимая функция для авторизации
function getUrlParameterValue(url, parameterName) {
    "use strict";

    var urlParameters  = url.substr(url.indexOf("#") + 1),
        parameterValue = "",
        index,
        temp;

    urlParameters = urlParameters.split("&");

    for (index = 0; index < urlParameters.length; index += 1) {
        temp = urlParameters[index].split("=");

        if (temp[0] === parameterName) {
            return temp[1];
        }
    }

    return parameterValue;
}
// Эта функция для авторизации, она берет параметры из url вкладки с адресом авторизации, и записывает в локальное 
//хранилище user_id и access_token
function listenerHandler(authenticationTabId) {
    "use strict";

    return function tabUpdateListener(tabId, changeInfo) {
 
        var vkAccessToken,
            vkAccessTokenExpiredFlag,
            user_id;

        if (tabId === authenticationTabId && changeInfo.url !== undefined && changeInfo.status === "loading") {

            if (changeInfo.url.indexOf('oauth.vk.com/blank.html') > -1) {
                authenticationTabId = null;
                chrome.tabs.onUpdated.removeListener(tabUpdateListener);

                vkAccessToken = getUrlParameterValue(changeInfo.url, 'access_token');
                user_id = getUrlParameterValue(changeInfo.url, 'user_id');
               // alert(user_id);
                if (vkAccessToken === undefined || vkAccessToken.length === undefined) {
                    displayeAnError('vk auth response problem', 'access_token length = 0 or vkAccessToken == undefined');
                    return;
                }

                vkAccessTokenExpiredFlag = Number(getUrlParameterValue(changeInfo.url, 'expires_in'));

                if (vkAccessTokenExpiredFlag !== 0) {
                    displayeAnError('vk auth response problem', 'vkAccessTokenExpiredFlag != 0' + vkAccessToken);
                    return;
                }
                var obj={};
				
                obj['vkaccess_token']=vkAccessToken;
                obj['user_id']=user_id;
				
                chrome.storage.local.set(obj);
				loadMusic();
				chrome.tabs.remove(tabId,function() { });
				 
            }
        }
    };
}
//Функция разлогинивания, очищает локальное хранилище и список песен в странице background.html
function Logout(){

 chrome.storage.local.clear();
 var back =  chrome.extension.getBackgroundPage().document;
 back.getElementById('playlist').innerHTML='';
 location.reload();
}
//Функция залогинивания. Создается новая вкладка хрома с адресом vkAuthenticationUrl, в случае если
// есть данных о залогиненом пользователи, вкладка не открывается. На эту вкладку вешается обработчик 
// события listenerHandler, который описан выше 
function Login(){

		   var vkClientId        = '3434493',
            vkRequestedScopes    = 'audio,offline',
            vkAuthenticationUrl  = 'https://oauth.vk.com/authorize?client_id=' + vkClientId + '&scope=' + vkRequestedScopes + '&redirect_uri=http%3A%2F%2Foauth.vk.com%2Fblank.html&display=page&response_type=token';
 
        chrome.storage.local.get({'vkaccess_token': {}}, function (items) {
			// проверяется есть ли данные о пользователе
             if (items.vkaccess_token.length === undefined) {
                chrome.tabs.create({url: vkAuthenticationUrl, selected: false}, function (tab) {
                   //вешается обработчик на событие обновления созданной вкладки
				   chrome.tabs.onUpdated.addListener(listenerHandler(tab.id));
			 
                });
 
            }else{
               var access_token = items.vkaccess_token;
  
        }
 
		});
		
}
/*******************************
 The function for sending requests to VK
 @param {string} query - url with query to VK.API
 @param {string} req_name - the name of request, 
							the response will be added to chrome storage
							with this name
********************************/
//Эта устаревшая функция обращения к методам ВК, ей лучше не пользоваться, просто оставила ее на всякий случай
function VKRequest(query, req_name){
	if(window.XMLHttpRequest){
			 
			var request = new XMLHttpRequest();
			request.onreadystatechange = function () {
				//document.getElementById("wrap").style.display = 'none';
				//document.getElementById("state").value = getRequestStateText(request.readyState);
			 
				// если выполнен
				if (request.readyState == 4) {
					//clearTimeout(abortRequest);
					//document.getElementById("wrap").value = request.status;
					//document.getElementById("wrap").value = request.statusText;
					// если успешно
					if (request.status == 200) {
					//	 document.getElementById("wrap").style.display = 'block';
						 var answer= JSON.parse(request.response); 
						// alert(req_name);
						var obj={};
						obj[req_name]=answer;
						obj[req_name+"_json"]=request.response;
				//		alert(request.response);
						chrome.storage.local.set(obj);
					} else {
						 alert("Не удалось получить данные:n" + request.statusText);
					}
					//	document.getElementById("loading").style.display = 'none';
					}
					// иначе, если идет загрузка или в процессе - показываем слой "Загружаются данные"
					else if (request.readyState == 3 || request.readyState == 1) {
					//document.getElementById("loading").style.display = 'block';
				}
				
			};
			request.open("GET", query, true);
			request.send(null);
 
			}else if(window.ActiveXObject){
				request=new ActiveXObject("Microsoft.XMLHHTP");
				if(request){
					request.onreadystatechange = processRequestChange(req_name);
					reguest.open("GET", query, true);
					request.send();
				}
		}

}
// функция необходимая для работы функции VKRequest
function getRequestStateText(code) {
	switch (code) {
		case 0: return "Uninitialized."; break;
		case 1: return "Loading..."; break;
		case 2: return "Loaded."; break;
		case 3: return "Interactive..."; break;
		case 4: return "Complete."; break;
	}
}
// используемая функция обращения к методам ВК.
/**
	url - адрес по которому обращаемся к API 
	data - параметры для запроса
	callback - функция выполняющаяся в случае успешного получения данных 
**/
function VKAjaxReq(url, data, callback){
		$.ajax({
				url: url, 
				data: data,
				dataType: 'json',
				success:  callback
				});
}
// Функция преобразующая секунды в минуты и секунды
function secToMinute(sec){
	var min = Math.floor(sec/60);
	var secs = Math.floor(sec%60);
	var len=secs+'';
	if(len.length<=1) secs='0'+secs;
	return min+":"+secs;
}

// Функция, которая загружает аудиозаписи со страницы пользователя, если он залогинен
	function loadMusic(){
	// обращение к переменным в хранилище хрома
	chrome.storage.local.get({'user_id': {},'vkaccess_token': {}}, function (items) {
			// проверка залогинен пользователь или нет
             if (items.vkaccess_token.length === undefined) {
				$("#log").html('Войти');
				 $("#log").val("login");
 
				}
				
			else{
				$("#log").html('Выйти');
				$("#log").val("logout");
				var playlist=document.getElementById('playlist');
				playlist.innerHTML="<img id='loading' src='css/img/loading.gif'>";
				
				var user_id,
                access_token;
				user_id=items.user_id;
				access_token=items.vkaccess_token;
				// back - это страница background.html, именно в ней хранятся аудиозаписи, и оттуда копируются уже в popup.html
				var back =  chrome.extension.getBackgroundPage().document;
				var playlist_back=back.getElementById('playlist');
					 //Проверяется если в странице background уже есть записи, то заново их не загружаем
					if(playlist_back.innerHTML==''){
					// Обращение к API, берем все аудио у пользователя
				VKAjaxReq('https://api.vk.com/method/audio.get','uid='+user_id+'&access_token='+access_token,function(result){
					var playlist=document.getElementById('playlist');
					var back =  chrome.extension.getBackgroundPage().document;
					var playlist_back=back.getElementById('playlist');
				//	if(playlist.innerHTML==''){
					var audio='';
					 var clas="sec";
				 
					for(var i=0; i<result.response.length;i++){
					if(i%2!=0){
						clas="nosec";
					}else{
						clas="sec";
					}
					 
						audio+='<li class="song '+clas+'" id="'+result.response[i].url+'"><input type="hidden" class="songURL" value="'+result.response[i].url+'"><i><img src="css/img/play.png"></i>'+result.response[i].artist+' - '+result.response[i].title+'<span class="duration">'+secToMinute(result.response[i].duration)+'</span></li><span class="addSong">+</span>';
					}
				playlist_back.innerHTML='<ul>'+audio+'</ul>'; 
				//  }
				playlist.innerHTML = playlist_back.innerHTML;
				 	actions();
				});
		}else{
		
		var tabs=document.getElementsByClassName('section')[0];
		//alert(tabs);
		//alert(back.getElementsByClassName('section')[0].innerHTML);
		tabs.innerHTML=back.getElementsByClassName('section')[0].innerHTML;
		//var playlist=document.getElementById('playlist');
		//playlist.innerHTML = playlist.innerHTML;
		actions();
		
			}
			}
 })
 }
 // Функция вставляющая необходимые для корректной работы popup
	function actions(){
		return $(document).ready(function(){
		// Popup скроллиться до места текущей песни
		 $(document).find('.box').each(function(){
			var song=$(document).find('.active_song')[0];
			if(song!=undefined){
				this.scrollTop= song.offsetTop-134;
			}
			
		 });
		 // Функция с логикой работы вкладок
			(function($) {
			$(function() {

			  $('ul.tabs').delegate('li:not(.current)', 'click', function() {
				$(this).addClass('current').siblings().removeClass('current')
				  .parents('div.section').find('div.box').eq($(this).index()).fadeIn(1000).siblings('div.box').hide();
			  })

			})
			})(jQuery)
			// Функция, которая добавляет список всех плэйлистов если нажать на кнопку + около каждой песни
		$('.addSong').click(function(){

			if($(this).siblings('.listsPop')[0]!=undefined){
				 $($(this).siblings('.listsPop')[0]).remove();
			}else{
			var playlists=$(document).find('.playlists');
			var block = document.createElement('div');
			block.className='listsPop';
			var list='<ul>';
			for(i=0; i<playlists.length;i++){
				list+='<li class="listAdd"><input type="hidden" class="listId" value="'+playlists[i].id+'">'+playlists[i].innerHTML+'</li>';
			}
			list+='</ul>';
			block.style.position='relative';
			block.style.display='block';
			//block.style.top=this.offsetTop+15;
			block.innerHTML=list;
			// Этот обработчик добавляет песню в выбранный плэйлист
			block.addEventListener('click', function(e){
				var back =  chrome.extension.getBackgroundPage().document;
				var song = $($(this).prev('.addSong')[0]).prev('.song')[0];
				var ths = e.target;
				var lstId = $(ths).find('.listId')[0].value;
				$(document).find('#'+lstId+'-box')[0].getElementsByTagName('ul')[0].innerHTML+='<li class="song" id="'+lstId+$(song).find('.songURL')[0].value+'">'+song.innerHTML+'</li>';
				$(back).find('#'+lstId+'-box')[0].getElementsByTagName('ul')[0].innerHTML+='<li class="song" id="'+lstId+$(song).find('.songURL')[0].value+'">'+song.innerHTML+'</li>';
				$(this).remove();
				$('.song').click(function(){

							var url=$(this).find('.songURL')[0].value;
							 
							var back =  chrome.extension.getBackgroundPage().document;
							 
							 
							if($(document).find('.active_song')!=undefined&&$(back).find('.active_song')!=undefined){
							 
								$($(document).find('.active_song')[0]).removeClass('active_song');
								$($(back).find('.active_song')[0]).removeClass('active_song');
							}
							//var player = document.getElementById('player');
							var backplayer = back.getElementById('player_back');
							 
							var id=$(this).attr('id');
							if(!$(this).hasClass('active_song')){
								$(this).addClass('active_song');
								$(back.getElementById(id)).addClass('active_song');
							}else{
								$(this).removeClass('active_song');
								$(back.getElementById(id)).removeClass('active_song');
								 
							}
							$(backplayer).removeClass('active');
							//player.setAttribute('src', url);
							backplayer.setAttribute('src', url);
							//play(player);
							play(backplayer);
						
						});
			});
			$(this).after(block);
			}
		} );
		/*$('.box').scroll(function(){
			var listsPop=$(document).find('.listsPop');
			for(i=0; i<listsPop.length; i++){
				$(listsPop[i]).remove();
			}
		});*/
	 
	 // Добавляет новую вкладку
		$('#addTab').click(function(){
			var back =  chrome.extension.getBackgroundPage().document;
			var newTab = document.createElement('li');
			var tabDiv =document.createElement('div');
			newTab.innerHTML="New playlist";
			newTab.className='playlists';
			newTab.id = 'new-playlist'+$(this).index();
			tabDiv.className='box';
			tabDiv.id = newTab.id+'-box';
		    tabDiv.innerHTML='<ul></ul>';
		 
		 // Позволяет при двойном щелчке переименовывать вкладку
				newTab.addEventListener('dblclick', function(){	
				var back =  chrome.extension.getBackgroundPage().document;				
				var name=document.createElement('input');
				 name.type='text';
				 name.className='changeName';
				 name.value=$(this).html();
				 name.style.height='16px';
				 name.style.width = $(this).css('width');
				 $(this).html(name);
				 name.focus();
				 name.addEventListener('blur',function(){
					var id=$(this).parent('.playlists').attr('id');
					var newName=this.value;
					
					$(back).find('#'+id)[0].innerHTML=newName;
					$(this).parent('.playlists').html(newName);
					
					//$(this).parent('.playlists').attr('id', $(this).val());
					//$(back).find('#'+id)[0].id=$(this).val();
				 });
			});   
		
			var newTab2 = document.createElement('li');
			var tabDiv2 =document.createElement('div');
			newTab2.innerHTML="New playlist";
			newTab2.className='playlists';
			newTab2.id = 'new-playlist'+$(this).index();
			tabDiv2.className='box';
			tabDiv2.id = newTab.id+'-box';
			tabDiv2.innerHTML='<ul></ul>';
			$(back).find('#addTab').last().before(newTab2);
			$(back).find('.box').last().after(tabDiv2);
				$(this).before(newTab);
			$(document).find('.box').last().after(tabDiv);
		 
		}); 
		
		$('.playlists').dblclick(function(){
			var name=document.createElement('input');
				 name.type='text';
				 name.className='changeName';
				 name.value=$(this).html();
				 name.style.height='16px';
				 name.style.width = $(this).css('width');
				 $(this).after(name);
				 name.focus();
				 name.addEventListener('blur',function(){
					$(this).closest('.playlists')[0].html($(this).val());
				 });
		});
		// Воспроизводит песню, на которую нажали и делает ее активной
				$('.song').click(function(){
				
							var url=$(this).find('.songURL')[0].value;
							var back =  chrome.extension.getBackgroundPage().document;

							if($(document).find('.active_song')!=undefined&&$(back).find('.active_song')!=undefined){
		 
								$($(document).find('.active_song')[0]).removeClass('active_song');
								$($(back).find('.active_song')[0]).removeClass('active_song');
							}
							//var player = document.getElementById('player');
							var backplayer = back.getElementById('player_back');
							 
							var id=$(this).attr('id');
							if(!$(this).hasClass('active_song')){
								$(this).addClass('active_song');
								$(back.getElementById(id)).addClass('active_song');
							}else{
								$(this).removeClass('active_song');
								$(back.getElementById(id)).removeClass('active_song');
							}
							$(backplayer).removeClass('active');
							//player.setAttribute('src', url);
							backplayer.setAttribute('src', url);
							//play(player);
							play(backplayer);
						
						});
		});	
			}
			// функция которая включает плеер, если он выключен, сюда передаеются элемент <audio></audio>
		function play(player){
			var playBut = document.getElementById('Button-Play');
			if(player.getAttribute('src')!=''&&!$(player).hasClass('active')){
				player.play();
				$(player).addClass('active');
				$(playBut).css('background-image',"url('css/img/pause.png')");
			}else{
				if($(player).hasClass('active')){
					player.pause();
					$(player).removeClass('active');
					$(playBut).css('background-image',"url('css/img/play.png')");
				}
			
			}
		
		}	
	 
		document.addEventListener('DOMContentLoaded', function () {
		
		document.getElementById('log').addEventListener('click',function(){
		if($(this).val()=='logout'){
			 Logout();
			 location.reload();
		}else{
		 
		Login();
			 
		 
		}
		});
		// Воспроизводит или останавливает проигрывание по наанию на кнопку Play/Pause
		document.getElementById('Button-Play').addEventListener('click', function(){
			var player = chrome.extension.getBackgroundPage().document.getElementById('player_back');
			play(player);
		});
		// Регулирует громкость
		document.getElementById('volume').addEventListener('change', function(){
			var player = chrome.extension.getBackgroundPage().document.getElementById('player_back');
			player.volume = this.value;
		
		});
		 
	 // Изменяет прогресс песни в элементе canvas
		document.getElementById('progress').addEventListener("click", function(e) {
                    //this might seem redundant, but this these are needed later - make global to remove these
                    var oAudio = chrome.extension.getBackgroundPage().document.getElementById('player_back');
                    var canvas = this;            

                    if (!e) {
                        e = window.event;
                    } //get the latest windows event if it isn't set
                    try {
                        //calculate the current time based on position of mouse cursor in canvas box
                        oAudio.currentTime = oAudio.duration * (e.offsetX / canvas.clientWidth);
                    }
                    catch (err) {
                    // Fail silently but show in F12 developer tools console
                        if (window.console && console.error("Error:" + err));
                    }
                });
            
		// показывает сколько песня играет	
		 document.getElementById('progress').addEventListener('change', function(){
			 var player = chrome.extension.getBackgroundPage().document.getElementById('player_back');
			 player.currentTime = this.value*player.duration;
		});
		// включает следующую песню при наатии на кнопку Next
		document.getElementById('Button-Next').addEventListener('click', function(){
			 var back = chrome.extension.getBackgroundPage().document;
			 var player = back.getElementById('player_back');
			// var id = player.getAttribute('src');
			 var currentSong = $(document).find('.active_song')[0];
			 var nextSong = ($(currentSong).nextAll('.song')[0]);
			 if(nextSong!=undefined){
				 $($(document).find('.active_song')[0]).removeClass('active_song');
				 var url = $(nextSong).find('.songURL')[0].value;			 
				 $(nextSong).addClass('active_song');
				 $($(back).find('.active_song')[0]).removeClass('active_song');
				 
				 $($(back).find('#'+nextSong.id)[0]).addClass('active_song');
				 player.setAttribute('src', url);
				 $(player).removeClass('active');
				 play(player);
			 }
		});
		// включает предыдущую песню при наатии на кнопку Previous
		document.getElementById('Button-Previous').addEventListener('click', function(){
			var back = chrome.extension.getBackgroundPage().document;
			 var player = back.getElementById('player_back');
			// var id = player.getAttribute('src');
			 var currentSong = $(document).find('.active_song')[0];
			 var prevSong = ($(currentSong).prevAll('.song')[0]);
			 if(prevSong!=undefined){
				 $($(document).find('.active_song')[0]).removeClass('active_song');
				 var url = $(prevSong).find('.songURL')[0].value;			 
				 $(prevSong).addClass('active_song');
				 $($(back).find('.active_song')[0]).removeClass('active_song');
				 $($(back).find('#'+prevSong.id)[0]).addClass('active_song');
				 player.setAttribute('src', url);
				 $(player).removeClass('active');
				 play(player);
			 }
		});
		// Устанавливает в backgraund значение элемента checked, который отвечает за рандомное воспроизведение
		$('#random').click(function(){
		var back = chrome.extension.getBackgroundPage().document;
			if($(this).attr('checked')){
				back.getElementById('random').checked=true;
			}else{
				back.getElementById('random').checked=false;
			}
		});
		var back = chrome.extension.getBackgroundPage().document;
		var player = back.getElementById('player_back');
		if(!player.paused){
			var but = document.getElementById('Button-Play');
			$(but).css('background',"url('css/img/pause.png')");
		}
	 
		if($(back).find('.active_song')[0]!=undefined){
			var idSong = $(back).find('.active_song')[0].id;
			$(document).find('.active_song').removeClass('.active_song');
			$(document.getElementById(idSong)).addClass('.active_song');
		}
		if(back.getElementById('random').checked){
			$('#random').attr('checked', true);
		}else{
			$('#random').attr('checked', false);
		}
	   
		loadMusic();
		
});
